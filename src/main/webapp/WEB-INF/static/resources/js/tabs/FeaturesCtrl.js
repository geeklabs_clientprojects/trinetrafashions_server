/**
 * 
 */
/**
 * 
 */

(function() {
	'use strict';
	var app = angular.module('TemplatesApp').controller('FeaturesCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session,UserInfoService) {

		  $scope.getAdds = function() {
  			$http({
  				url : "/add/get",
  				method : 'GET',
  				dataType : 'json'
  			}).then(function(dataResponse) {
  				var data = dataResponse.data;
  				$scope.adds = data;
  			});
  		};		
  		  $scope.getAdds();
	});
})();