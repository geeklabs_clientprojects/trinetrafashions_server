(function() {
	'use strict';
	angular.module('TemplatesApp').controller('SubCategoryCtrl', function($rootScope, $scope, $state, $http, notify, AuthService, Session) {
		debugger;
		$scope.subCategoryVO = {};
		$scope.subCategories = [];
		$scope.categories = [];
		
		$scope.showAddSubCategoryModal = function() {
		debugger;
			$scope.clearModal();
			$(".add-subCategory-modal").modal("show");
		};
		
		$scope.clearModal = function() {
			$scope.subCategoryVO = {};
			$(".add-subCategory-modal").modal("hide");
		};
		
		// Update event
		$scope.editSubCategory = function(subCategoryId) {
			debugger;
			$scope.subCategoryVO = $scope.subCategories[subCategoryId];
			$(".add-subCategory-modal").modal("show");
		};
		
		// delete subCategory
		$scope.deleteSubCategory = function(subCategoryId) {
			debugger;
			var result = confirm("Are you sure want to delete?");
			if (result == true) {
			$http({
				url : "/subCategory/delete/" + subCategoryId,
				method : 'DELETE',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('SubCategory deleted successfully');
					$scope.getSubCategories();
				} else {
					notify('Error occured While deleting Sub Category');
				}
			});
			}
		};
		
		$scope.getSubCategories = function() {
			debugger;
			$http({
				url : "/subCategory/get/",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.subCategories = data;
			});
		};
		
		$scope.saveSubCategory = function() {
			debugger;
			$http({
				url : "/subCategory/add",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : $scope.subCategoryVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Sub Category saved successfully');
					$(".add-subCategory-modal").modal("hide");
					$scope.getSubCategories();
					$scope.clearModal();
				} else {
					notify('Error occured while saving Sub Category');
				}
			});
		};
		
		$scope.getCategories = function() {
			debugger;
			$http({
				url : "/category/get/",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.categories = data;
			});
		};
		
		$scope.updateCategory = function(){
			debugger;
			var categoryDetails = $scope.categoryItem;
			$scope.subCategoryVO.categoryName = categoryDetails.categoryName;
		}
		
		$scope.getSubCategories();
		$scope.getCategories();
		
		});
})();