angular.module('TemplatesApp').controller('ApplicationCtrl', function($scope, $rootScope, $state, USER_ROLES, AuthService, Session, $http) {
	$scope.currentUser = null;
	$scope.emailId = '';
	$scope.userRoles = USER_ROLES;
	$scope.isAuthorized = AuthService.isAuthorized;
	$scope.isCategoriesVisible = true;
	$scope.isTemplatesVisible = false;
	$scope.isBackVisible =  false;

	$scope.setCurrentUser = function(user) {
		$scope.currentUser = user;
	};
	
	 $scope.filterList = function() {
         if($scope.templateName){
         var inputSearchText = $scope.templateName;
         $http({
             url : "/template/get/inputText/" + inputSearchText,
             method : 'GET',
             dataType : 'json'
         }).then(function(dataResponse) {
             var data = dataResponse.data;
             $scope.templates = data;
             $scope.isTemplatesVisible = true;
     		 $scope.isCategoriesVisible = false;
     		 $scope.isBackVisible =  true;
               });
             }
         };
         
         $scope.getTemplates = function(categoryId) {
         	$http({
 				url : "/template/get/template/" + categoryId,
 				method : 'GET',
 				dataType : 'json'
 			}).then(function(dataResponse) {
 				debugger;
 				var data = dataResponse.data;
 				$scope.templates = data;
 				$scope.isCategoriesVisible = false; 
 				$scope.isTemplatesVisible = true;
 				$scope.isBackVisible =  true;
 				});
         	};
         
//         $scope.$emit('templates',$scope.templates);
         
		$scope.logout = function() {
			// TODO - > hit server to logout and then do below
			Session.destroy();
			$scope.setCurrentUser(null);
			$state.transitionTo("index");
		};
	
});
