(function() {
	'use strict';
	angular.module('TemplatesApp').controller('CategoryCtrl', function($rootScope, $scope, $state, $http, notify, AuthService, Session, Upload) {
		debugger;
		$scope.categoryVO = {};
		$scope.categories = [];
		$scope.categoryId = 0;
		
		
		$scope.showAddCategoryModal = function() {
			$scope.clearModal();
			$(".add-category-modal").modal("show");
		};
		
		$scope.clearModal = function() {
			$(".add-category-modal").modal("hide");
			$scope.categoryVO = {};
		};
		
		// Update category
		$scope.editCategory = function(index) {
			debugger;
			var category = $scope.categories[index];
			 $scope.categoryId = category.id;
			 $scope.categoryVO.categoryName = category.categoryName;
			$(".add-category-modal").modal("show");
		};
		
		$scope.saveCategoryWithFile = function() {
	    	debugger;
	      if ($scope.file && !$scope.file.$error) {
	        $scope.upload($scope.file);
	        $scope.clearModal();
	        $scope.getCategories();
	      } else {
	    	  alert('Please choose a file before Upload and Save');
	      }
	    };
	    
	 // upload on file select or drop
	    $scope.upload = function (file) {
	    	debugger;
	        Upload.upload({
	            url: '/category/upload',
	            fields: {'categoryName': $scope.categoryVO.categoryName, 'categoryId': $scope.categoryId},
	            file: file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	        }).success(function (data, status, headers, config) {
	        	debugger;
	        	if (status == "success") {
					notify('Category Added successfully');
				} else {
					notify('Error occured While adding Category');
				}
	            alert('File ' + config.file.name + ' uploaded successfully.');
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	            // notify('Error occured While adding Category');
	            // alert('Failed to upload file ' + config.file.name);
	        })
	        $scope.getCategories();
	    };
		
		// delete category
		$scope.deleteCategory = function(categoryId) {
			debugger;
			var result = confirm("Are you sure want to delete?");
			if (result == true) {
			$http({
				url : "/category/delete/" + categoryId,
				method : 'DELETE',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Category deleted successfully');
					$scope.getCategories();
				} else {
					notify('Error occured While deleting Category');
				}
			});
			}
		};
		
		$scope.getCategories = function() {
			debugger;
			$http({
				url : "/category/get/",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.categories = data;
			});
		};
		
		$scope.saveCategory = function() {
			debugger;
			$http({
				url : "/category/add",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : $scope.categoryVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Category saved successfully');
					$(".add-category-modal").modal("hide");
					$scope.getCategories();
					$scope.clearModal();
				} else {
					notify('Error occured while saving Category');
				}
			});
		};
		
		$scope.getCategories();
		
		});
})();