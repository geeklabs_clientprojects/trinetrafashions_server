'use strict';

// http://ngmodules.org/modules/ngAutocomplete
// https://github.com/mattlewis92/angular-bootstrap-calendar
angular.module('TemplatesApp', [ 'ngAnimate', 'ui.router', 'restangular'
                     , 'cgNotify', 'ngAutocomplete', 'ui.bootstrap', 
                     'mwl.calendar', 'angular-loading-bar', 'ui.bootstrap.datetimepicker', 'ngFileUpload']).config(
		function($stateProvider, $urlRouterProvider, RestangularProvider, 
				$httpProvider, USER_ROLES) {
			$urlRouterProvider.otherwise("/index");
			
			$stateProvider.state('index', {
                url : "/index",
               templateUrl : "static/views/index.html",
               controller : 'IndexCtrl'
           });

			$stateProvider.state('about', {
				url : "/about",
				templateUrl : "static/views/about.html"
			});

            $stateProvider.state('services', {
				url : "/services",
				templateUrl : "static/views/services.html"
			});

            $stateProvider.state('features', {
				url : "/features",
				templateUrl : "static/views/features.html"
			});
            
            $stateProvider.state('contact', {
            	url : "/contact",
            	templateUrl : "static/views/contact.html"
            });

            $stateProvider.state('templateView', {
				url : "/templateView",
				templateUrl : "static/views/template/templateView.html",
				controller : 'templateViewCtrl'
			});

			$stateProvider.state('login', {
				url : "/login",
				templateUrl : "static/views/auth.html",
				controller : 'AuthCtrl'
			});

			$stateProvider.state('user', {
				url : "/home",
				abstract : true,
				templateUrl : "static/views/home/homeleftsidenav.html",
				controller : 'HomeCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			
			$stateProvider.state('user.welcome', {
				url : "/welcome",
				templateUrl : "static/views/home/homecontent.html",
				controller : 'HomeCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			$stateProvider.state('user.category', {
				url : "/category",
				templateUrl : "static/views/category/category.html",
				controller : 'CategoryCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			$stateProvider.state('user.subCategory', {
				url : "/subCategory",
				templateUrl : "static/views/category/subcategory.html",
				controller : 'SubCategoryCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			$stateProvider.state('user.template', {
				url : "/template",
				templateUrl : "static/views/template/template.html",
				controller : 'TemplateCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			$stateProvider.state('user.add', {
				url : "/add",
				templateUrl : "static/views/add/add.html",
				controller : 'AddCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});
			/*$stateProvider.state('user.slider', {
				url : "/slider",
				templateUrl : "static/views/add/slider.html",
				controller : 'SliderCtrl',
				data : {
					authorizedRoles : [ USER_ROLES.admin ]
				}
			});*/
			
			// It is very easy to get clean URLs and remove the hash tag from the URL.
			// $locationProvider.html5Mode(true);

			RestangularProvider.setBaseUrl('/templates');
			// RestangularProvider.setDefaultRequestParams({ apiKey:
			// '4f847ad3e4b08a2eed5f3b54' })
			RestangularProvider.setRestangularFields({
				id : 'id'
			});
			RestangularProvider.setRequestInterceptor(function(elem, operation, what) {
				// can have additional verification on logged in user for
				// specific
				// function.
			});

		}).run(function($rootScope, $state, $stateParams, notify, AuthService, AUTH_EVENTS) {
	// $rootScope.userInfo = '';
	$rootScope.$state = $state;
	$rootScope.$stateParams = $stateParams;
	$rootScope.notify = notify;

	// HTTP For role based authorization
	$rootScope.$on("$stateChangeStart", function(event, nextToState) {
		if (nextToState.data) {
			var authorizedRoles = nextToState.data.authorizedRoles;
			if (!AuthService.isAuthorized(authorizedRoles)) {
				event.preventDefault();
				if (AuthService.isAuthenticated()) {
					// user is not allowed
					$rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
				} else {
					// user is not logged in
					$rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
				}
			}
		}
	});

});
