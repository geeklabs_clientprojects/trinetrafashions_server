(function() {
	'use strict';
	angular.module('TemplatesApp').controller('AddCtrl', function($rootScope, $scope, $state, $http, notify, AuthService, Session, Upload) {
		debugger;
		$scope.addVO = {};
		$scope.adds = [];
		$scope.addId = 0;
		
		
		$scope.showAddaddModal = function() {
		debugger;
			$scope.clearModal();
			$(".add-add-modal").modal("show");
		};
		
		$scope.clearModal = function() {
			debugger;
			$scope.addVO = {};
			$(".add-add-modal").modal("hide");
		};
		
		// Update add
		$scope.editAdd = function(index) {
			debugger;
			var add = $scope.adds[index];
			 $scope.addId = add.id;
			 $scope.addVO.addName = add.addName;
			 $scope.addVO.addUrl = add.addUrl;
			$(".add-add-modal").modal("show");
		};
		
		$scope.saveAddWithFile = function() {
	    	debugger;
	      if ($scope.file && !$scope.file.$error) {
	        $scope.upload($scope.file);
	        $scope.clearModal();
	        $scope.getAdds();
	      } else {
	    	  alert('Please choose a file before Upload and Save');
	      }
	    };
	    
	 // delete add
		$scope.deleteAdd = function(addId) {
			debugger;
			var result = confirm("Are you sure want to delete?");
			if (result == true) {
			$http({
				url : "/add/delete/" + addId,
				method : 'DELETE',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('add deleted successfully');
					$scope.getAdds();
				} else {
					notify('Error occured While deleting add');
				}
			});
			}
		};
	    
	 // upload on file select or drop
	    $scope.upload = function (file) {
	    	debugger;
	        Upload.upload({
	            url: '/add/upload',
	            fields: {'addName': $scope.addVO.addName, 'addUrl': $scope.addVO.addUrl, 'addId': $scope.addId},
	            file: file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	        }).success(function (data, status, headers, config) {
	        	debugger;
	        	if (status == "success") {
					notify('add Added successfully');
					alert('File ' + config.file.name + ' uploaded successfully.');
				} else {
					notify('Error occured While adding add');
					 $scope.getAdds();
				}
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	            // notify('Error occured while adding add');
	            // alert('Failed to upload file ' + config.file.name);
	        })
	    };
		
		$scope.getAdds = function() {
			debugger;
			$http({
				url : "/add/get",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.adds = data;
			});
		};
		$scope.getAdds();
		});
})();