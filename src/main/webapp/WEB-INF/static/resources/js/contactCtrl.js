//Fetches Model and minds to view
//Can use services to make http calls.

// Inspired from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

(function() {
	'use strict';
	var app = angular.module('TemplatesApp').controller('LoginCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session) {
		debugger;

		 $scope.markers = [
                {
                   "latitude":33.22,
                   "longitude":35.33
                }
            ];

            var mapOptions = {
                zoom: 3,
                center: new google.maps.LatLng(40.0000, -98.0000),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            }

            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);

            var infoWindow = new google.maps.InfoWindow();

            var createMarker = function (info){

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    position: new google.maps.LatLng(info.lat, info.long),
                    title: info.city
                });
                marker.content = '<div class="infoWindowContent">' + info.desc + '</div>';

                google.maps.event.addListener(marker, 'click', function(){
                    infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                    infoWindow.open($scope.map, marker);
                });

                $scope.markers.push(marker);

            }

            for (i = 0; i < cities.length; i++){
                createMarker(cities[i]);
            }

            $scope.openInfoWindow = function(e, selectedMarker){
                e.preventDefault();
                google.maps.event.trigger(selectedMarker, 'click');
            }
	});
})();