//Fetches Model and minds to view
//Can use services to make http calls.

// Inspired from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

(function() {
	'use strict';
	var app = angular.module('TemplatesApp').controller('AuthCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session) {
		debugger;
		$scope.loginUser = function(userVO) {
			debugger;
			var body = "username=" + userVO.username + "&password=" + userVO.password;
			$http({
				url : "/user/login",
				method : 'POST',
				'headers' : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				},
				data : body
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Successfully logged in.');
					$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
					$scope.setCurrentUser(userVO);
					// FIXME
					Session.create("sessionId", dataResponse.data.userId, dataResponse.data.userRole);
					$state.transitionTo("user.welcome");
				} else {
					// TODO - notify Warning color ??
					$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					notify('Username or Password is invalid.');
				}
			});

		};
	});
})();