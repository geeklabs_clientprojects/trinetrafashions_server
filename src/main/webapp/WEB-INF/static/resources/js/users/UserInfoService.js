angular.module('TemplatesApp').factory('UserInfoService', function() {
	var selectedTemp;

	function set(data) {
		selectedTemp = data;
	}
	function get() {
		return selectedTemp;
	};

	return {
		set : set,
		get : get
	};
});
