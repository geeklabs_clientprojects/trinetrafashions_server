(function() {
	'use strict';
	angular.module('TemplatesApp').controller('UserCtrl', function($rootScope, $scope, $state, $http, notify, UserInfoService, AUTH_EVENTS, AuthService, Session) {
		debugger;
		$scope.user = [];
		$scope.userId = '';
		
		$scope.viewUserInfo = function(userId) {
			debugger;
			$scope.userId = userId;
			$http({
				url : "/user/get/" + userId,
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.user = data;
				UserInfoService.set($scope.user);
				$state.transitionTo("user.userinfo.profile");
			});
		};
					
		});
})();