package com.geeklabs.templates.repo;

import com.geeklabs.templates.domain.User;
import com.geeklabs.templates.repo.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User> {
	
	public User getUserByEmail(String userEmail);
}
