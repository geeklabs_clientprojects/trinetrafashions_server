
package com.geeklabs.templates.repo.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.templates.domain.Template;
import com.geeklabs.templates.repo.TemplateRepository;
import com.geeklabs.templates.repo.objectify.AbstractObjectifyCRUDRepository;
import com.geeklabs.templates.util.Pageable;
import com.googlecode.objectify.Objectify;

@Repository
public class TemplateRepositoryImpl extends AbstractObjectifyCRUDRepository<Template>implements TemplateRepository {

	@Autowired
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	public TemplateRepositoryImpl() {
		super(Template.class);
	}

	@Override
	public List<Template> getAllTemplatesByCategoryAndDate(Pageable pageable, int month, int day, Long categoryId) {

		List<Template> allTemplates = new ArrayList<>();

		if (categoryId == null) {
			return objectify.load()
							.type(Template.class)
							.filter("categoryId", categoryId)
//							.order("templateName")
							.offset(pageable.getOffset())
							.limit(Pageable.LIMIT)
							.list();
		}

		// Get all templates where category matched
		if (categoryId != null) {
			addRecordsByCategory(pageable, month, day, categoryId, allTemplates);
		}

		return allTemplates;
	}

	private void addRecordsByCategory(Pageable pageable, int month, int day, Long categoryId,
			List<Template> allTemplates) {
		@SuppressWarnings("static-access")
		List<Template> templates = objectify.load()
											.type(Template.class)
											.filter("categoryId", categoryId)
											.offset(pageable.getOffset())
											.limit(pageable.LIMIT)
											.list();

		if (templates != null && !templates.isEmpty()) {
			allTemplates.addAll(templates);
		}
	}

	@Override
	public List<Template> findAllTemplatesByPagable(Pageable pageable) {
		return 	objectify.load()
						 .type(Template.class)
						 .limit(Pageable.LIMIT)
//						 .order("templateName")
						 .offset(pageable.getOffset())
						 .list();
		}
	
	@Override
	public List<Template> getTemplates(Pageable pageable, Long categoryId) {
		return 	objectify.load()
						 .type(Template.class)
						 .limit(Pageable.LIMIT)
						 .filter("categoryId", categoryId)
						 .offset(pageable.getOffset())
//						 .order("templateName")
						 .list();
			}
	
	@Override
	public List<Template> getTemplatesByCategory(Long categoryId) {

		List<Template> allTemplates = new ArrayList<>();

		if (categoryId == null) {
			return objectify.load()
							.type(Template.class)
							.filter("categoryId", categoryId)
//							.order("templateName")
							.list();
		}
		// Get all templates where category matched
		if (categoryId != null) {
			addRecordsByCategory(categoryId, allTemplates);
		}

		return allTemplates;
	}

	private void addRecordsByCategory(Long categoryId, List<Template> allTemplates) {
		List<Template> templates = objectify.load()
											.type(Template.class)
											.filter("categoryId", categoryId)
											.list();

		if (templates != null && !templates.isEmpty()) {
			allTemplates.addAll(templates);
		}
	}
	
	@Override
	public List<Template> findAllTemplatesByTemplateName(String inputSearchText) {
		return objectify.load()
						.type(Template.class)
						.filter("templateName >=", inputSearchText)
						.filter("templateName <", inputSearchText + "\uFFFD") 
						.list();
	}
}
