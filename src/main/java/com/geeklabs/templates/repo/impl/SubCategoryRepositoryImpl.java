package com.geeklabs.templates.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.templates.domain.SubCategory;
import com.geeklabs.templates.repo.SubCategoryRepository;
import com.geeklabs.templates.repo.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class SubCategoryRepositoryImpl extends AbstractObjectifyCRUDRepository<SubCategory>
		implements SubCategoryRepository {

	@Autowired
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	public SubCategoryRepositoryImpl() {
		super(SubCategory.class);
	}

}
