package com.geeklabs.templates.repo.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.templates.domain.Add;
import com.geeklabs.templates.repo.AddRepository;
import com.geeklabs.templates.repo.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class AddRepositoryImpl extends AbstractObjectifyCRUDRepository<Add> implements AddRepository{

	@Autowired 
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	public AddRepositoryImpl(){
		super(Add.class);
	}
	
}
