package com.geeklabs.templates.repo.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.templates.domain.Category;
import com.geeklabs.templates.repo.CategoryRepository;
import com.geeklabs.templates.repo.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class CategoryRepositoryImpl extends AbstractObjectifyCRUDRepository<Category> implements CategoryRepository{

	@Autowired 
	private Objectify objectify;

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	public CategoryRepositoryImpl() {
		super(Category.class);
	}

	@Override
	public List<Category> findAllByCategoryName() {
		return 	objectify.load()
				 .type(Category.class)
//				 .order("categoryName")
				 .list();
	}

}
