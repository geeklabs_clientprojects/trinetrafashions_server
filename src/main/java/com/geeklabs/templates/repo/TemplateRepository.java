package com.geeklabs.templates.repo;

import java.util.List;

import com.geeklabs.templates.domain.Template;
import com.geeklabs.templates.repo.objectify.ObjectifyCRUDRepository;
import com.geeklabs.templates.util.Pageable;

public interface TemplateRepository extends ObjectifyCRUDRepository<Template>{

	List<Template> getAllTemplatesByCategoryAndDate(Pageable pageable, int month, int day, Long categoryId);

	List<Template> findAllTemplatesByPagable(Pageable pageable);

	List<Template> getTemplates(Pageable pageable, Long categoryId);

	List<Template> getTemplatesByCategory(Long categoryId);

	List<Template> findAllTemplatesByTemplateName(String inputSearchText);

}
