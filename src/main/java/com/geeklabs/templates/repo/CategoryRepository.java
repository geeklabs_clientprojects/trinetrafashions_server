package com.geeklabs.templates.repo;

import java.util.List;

import com.geeklabs.templates.domain.Category;
import com.geeklabs.templates.repo.objectify.ObjectifyCRUDRepository;

public interface CategoryRepository extends ObjectifyCRUDRepository<Category>{

	List<Category> findAllByCategoryName();

}
