package com.geeklabs.templates.repo;

import com.geeklabs.templates.domain.SubCategory;
import com.geeklabs.templates.repo.objectify.ObjectifyCRUDRepository;

public interface SubCategoryRepository extends ObjectifyCRUDRepository<SubCategory> {

}
