package com.geeklabs.templates.repo;

import com.geeklabs.templates.domain.Add;
import com.geeklabs.templates.repo.objectify.ObjectifyCRUDRepository;

public interface AddRepository extends ObjectifyCRUDRepository<Add> {

}
