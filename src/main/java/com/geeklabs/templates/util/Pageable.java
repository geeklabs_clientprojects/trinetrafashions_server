package com.geeklabs.templates.util;

public class Pageable {
	public static final int LIMIT = 10;
	private int offset;
	
	public int getOffset() {
		return offset * LIMIT;
	}
	
	public void setOffset(int offset) {
		this.offset = offset;
	}	
}