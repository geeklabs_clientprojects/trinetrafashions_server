package com.geeklabs.templates.util;
public class ResponseStatus {

	private String status="error";
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
}
