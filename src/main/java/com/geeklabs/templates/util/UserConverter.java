package com.geeklabs.templates.util;

import com.geeklabs.templates.domain.User;
import com.geeklabs.templates.dto.UserDto;

public class UserConverter {

	public static UserDto convertUserToUserDto(User user) {

		UserDto userDto = new UserDto();
		userDto.setFirstName(user.getFirstName());
		userDto.setLastName(user.getLastName());
		userDto.setEmail(user.getEmail());
		userDto.setContact(user.getContact());
		userDto.setUserName(user.getUserName());
		userDto.setPassword(user.getPassword());

		return userDto;
	}

	public static User convertUserDtoToUser(UserDto userDto) {

		User user = new User();

		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		user.setContact(userDto.getContact());
		user.setUserName(userDto.getUserName());
		user.setPassword(userDto.getPassword());

		return user;
	}
}
