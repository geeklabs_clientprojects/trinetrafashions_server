package com.geeklabs.templates.util;

public class RequestMapper {

	/*User*/
	public static final String USER = "/user";
	public final static String USER_LOGIN_SUCCESS = "/login/success";
	public final static String USER_LOGIN_ERROR = "/login/error";
	public static final String IS_ACTIVATED = "/activate";
	
	/* my account */
	public static final String MY_ACCOUNT = "/myAccount";
	public static final String MY_ACCOUNT_GET_USER = "/myAccount/getUserInfo";

	/** Admin **/
	public static final String ADMIN = "/admin";
	public static final String SETUP = "/admin/setup";
	
	/** Document **/
	public static final String DOCUMENT = "/doc";
	public static final String GET_DOCUMENT = "/get/{identifier}";
	
	/*Template*/
	public static final String TEMPLATE = "/template";
	public static final String UPLOAD = "/upload";
	public static final String GET_TEMPLATES_BY_CATEGORY = "/get/{currentPage}/{categoryId}";
	public static final String GET_TEMPLATES_BY_CATEGORY_FOR_SERVER = "/get/template/{categoryId}";
	public static final String GET_TEMPLATES = "/get/{currentPage}";
	public static final String GET_TEMPLATES_FOR_SERVER = "/get";
	public static final String DELETE_TEMPLATE = "/delete/{templateId}";
	public static final String GET_ALL_TEMPLATES_BY_NAME = "/get/inputText/{inputSearchText}";
	
	/*Category*/
	public static final String CATEGORY = "/category";
	public static final String ADD_CATEGORY = "/add";
	public static final String GET_CATEGORY = "/get";
	public static final String DELETE_CATEGORY = "/delete/{categoryId}";
	
	/*Sub Category*/
	public static final String SUB_CATEGORY = "/subCategory";
	public static final String ADD_SUB_CATEGORY = "/add";
	public static final String GET_SUB_CATEGORY = "/get";
	public static final String DELETE_SUB_CATEGORY = "/delete/{categoryId}";

	/*adds*/
	public static final String ADD = "/add";
	public static final String GET_ADD = "/get";
	public static final String DELETE_ADD = "/delete/{addId}";
}
