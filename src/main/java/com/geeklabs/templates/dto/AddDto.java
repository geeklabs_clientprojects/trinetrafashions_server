package com.geeklabs.templates.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class AddDto {

	private Long id;
	private String addName;
	private Date createdDate;
	private MultipartFile[] addPic;
	private String addPicPath;
	private String addUrl;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getAddName() {
		return addName;
	}
	public void setAddName(String addName) {
		this.addName = addName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public MultipartFile[] getAddPic() {
		return addPic;
	}
	public void setAddPic(MultipartFile[] addPic) {
		this.addPic = addPic;
	}
	public String getAddPicPath() {
		return addPicPath;
	}
	public void setAddPicPath(String addPicPath) {
		this.addPicPath = addPicPath;
	}
	public String getAddUrl() {
		return addUrl;
	}
	public void setAddUrl(String addUrl) {
		this.addUrl = addUrl;
	}
	
	
}
