package com.geeklabs.templates.dto;

import org.springframework.web.multipart.MultipartFile;

public class UserDto {

	private Long id;
	private String email;
	private String firstName;
	private String lastName;
	private String userName;
	private String password;
	private Long contact;
	private MultipartFile[] profilePic;
	private String profilePicPath;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Long getContact() {
		return contact;
	}
	public void setContact(Long contact) {
		this.contact = contact;
	}
	public MultipartFile[] getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(MultipartFile[] profilePic) {
		this.profilePic = profilePic;
	}
	public String getProfilePicPath() {
		return profilePicPath;
	}
	public void setProfilePicPath(String profilePicPath) {
		this.profilePicPath = profilePicPath;
	}
	
}
