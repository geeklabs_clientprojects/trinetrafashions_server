package com.geeklabs.templates.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class CategoryDto {

	private Long id;
	private String categoryName;
	private Date createdDate;
	private MultipartFile[] categoryPic;
	private String categoryPicPath;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public MultipartFile[] getCategoryPic() {
		return categoryPic;
	}
	public void setCategoryPic(MultipartFile[] categoryPic) {
		this.categoryPic = categoryPic;
	}
	public String getCategoryPicPath() {
		return categoryPicPath;
	}
	public void setCategoryPicPath(String categoryPicPath) {
		this.categoryPicPath = categoryPicPath;
	}
	
}
