package com.geeklabs.templates.dto;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

public class TemplateDto {

	private Long id;
	private String templateName;
	private Long categoryId;
	private String categoryName;
	private MultipartFile[] templatePic;
	private String templatePicPath;
	private Date createdDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public Long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}
	public String getTemplatePicPath() {
		return templatePicPath;
	}
	public void setTemplatePicPath(String templatePicPath) {
		this.templatePicPath = templatePicPath;
	}
	public MultipartFile[] getTemplatePic() {
		return templatePic;
	}
	public void setTemplatePic(MultipartFile[] templatePic) {
		this.templatePic = templatePic;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
}
