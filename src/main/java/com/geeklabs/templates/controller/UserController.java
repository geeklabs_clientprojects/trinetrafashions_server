package com.geeklabs.templates.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.templates.dto.LoginSuccessDto;
import com.geeklabs.templates.service.UserService;
import com.geeklabs.templates.util.RequestMapper;
import com.geeklabs.templates.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.USER)
public class UserController {

	@Autowired
	UserService userService;

	@ResponseBody
	@RequestMapping(value = RequestMapper.USER_LOGIN_SUCCESS, method = RequestMethod.GET,produces = "application/json")
	public LoginSuccessDto loginSuccess(Principal principal) {
		//CustomUserDetails userDetails = (CustomUserDetails) ((Authentication) principal).getPrincipal();
		LoginSuccessDto loginSuccessDto = new LoginSuccessDto();
		//loginSuccessDto.setUserId(userDetails.getId());
		loginSuccessDto.setUserName("geek");
		//loginSuccessDto.setEmail(userDetails.getEmail());
		loginSuccessDto.setUserRole("SYSTEM_ADMIN");
		loginSuccessDto.setStatus("success");
		return loginSuccessDto;
	}

	@RequestMapping(value = RequestMapper.USER_LOGIN_ERROR, method = RequestMethod.GET)
	public ResponseStatus loginFailure(Model map) {

		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("failure");
		return responseStatus;
	}

	@RequestMapping(value = RequestMapper.IS_ACTIVATED, method = RequestMethod.GET)
	public String isActivated(@RequestParam String activate, RedirectAttributes redirectAttributes) {
		return "redirect:/";
	}

	@RequestMapping(value = RequestMapper.SETUP, method = RequestMethod.GET)
	public String setup() {
		userService.setupAppUser();
//		return "redirect:/static/views/login";
		return "redirect:/#/index";
	}

}
