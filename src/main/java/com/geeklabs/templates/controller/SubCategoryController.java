package com.geeklabs.templates.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.templates.dto.SubCategoryDto;
import com.geeklabs.templates.service.SubCategoryService;
import com.geeklabs.templates.util.RequestMapper;
import com.geeklabs.templates.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.SUB_CATEGORY)
public class SubCategoryController {

	@Autowired
	private SubCategoryService subCategoryService;

	@ResponseBody
	@RequestMapping(value = RequestMapper.ADD_SUB_CATEGORY, method = RequestMethod.POST, produces = "application/json")
	public ResponseStatus addSubCategory(@RequestBody SubCategoryDto subCategoryDto) {
		ResponseStatus responseStatus = subCategoryService.saveSubCategory(subCategoryDto);
		return responseStatus;

	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_SUB_CATEGORY, method = RequestMethod.GET, produces = "application/json")
	public java.util.List<SubCategoryDto> getSubCategory() {
		return subCategoryService.getAllSubCategories();
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.DELETE_SUB_CATEGORY, method = RequestMethod.DELETE)
	public ResponseStatus deleteSubCategory(@PathVariable Long categoryId) {
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus = subCategoryService.deleteSubCategory(categoryId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

}
