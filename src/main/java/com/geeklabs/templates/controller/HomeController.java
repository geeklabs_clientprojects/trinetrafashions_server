package com.geeklabs.templates.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {

	// @Autowired
	// private HomeService homeService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String login(Model map) {
		return "static/views/login";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String home(Model map) {
		return "home";
	}
}