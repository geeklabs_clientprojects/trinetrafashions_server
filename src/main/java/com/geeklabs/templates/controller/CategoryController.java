package com.geeklabs.templates.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.templates.dto.CategoryDto;
import com.geeklabs.templates.service.CategoryService;
import com.geeklabs.templates.util.RequestMapper;
import com.geeklabs.templates.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.CATEGORY)
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@RequestMapping(value = RequestMapper.UPLOAD, produces = "application/json")
	public ResponseStatus saveCategoryWithImage(@ModelAttribute("category") CategoryDto categoryDto,
			@RequestParam("categoryName") String categoryName,@RequestParam("categoryId") Long categoryId, @RequestParam("file") MultipartFile file,
			final RedirectAttributes redirectAttributes, Model map) throws IOException {
		// Save template pic
		return categoryService.saveCategory(categoryDto, file, categoryName, categoryId);
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_CATEGORY, method = RequestMethod.GET, produces = "application/json")
	public List<CategoryDto> getCategory() {
		return categoryService.getAllCategories();
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.DELETE_CATEGORY, method = RequestMethod.DELETE)
	public ResponseStatus delete(@PathVariable Long categoryId) {
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus = categoryService.deleteCategory(categoryId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

}
