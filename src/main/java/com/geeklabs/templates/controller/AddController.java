package com.geeklabs.templates.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.templates.dto.AddDto;
import com.geeklabs.templates.service.AddService;
import com.geeklabs.templates.util.RequestMapper;
import com.geeklabs.templates.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.ADD)
public class AddController {

	@Autowired
	private AddService addService;

	@RequestMapping(value = RequestMapper.UPLOAD, produces = "application/json")
	public ResponseStatus saveAddWithImage(@ModelAttribute("add") AddDto addDto,
			@RequestParam("addName") String addName,@RequestParam("addUrl") String addUrl, @RequestParam("addId") Long addId, @RequestParam("file") MultipartFile file,
			final RedirectAttributes redirectAttributes, Model map) throws IOException {
		// Save template pic
		return addService.saveAdd(addDto, file, addName, addUrl, addId);
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_ADD, method = RequestMethod.GET, produces = "application/json")
	public List<AddDto> getAdd() {
		return addService.getAllAdds();
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.DELETE_ADD, method = RequestMethod.DELETE)
	public ResponseStatus delete(@PathVariable Long addId) {
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus = addService.deleteAdd(addId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

}
