package com.geeklabs.templates.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.geeklabs.templates.dto.TemplateDto;
import com.geeklabs.templates.service.TemplateService;
import com.geeklabs.templates.util.Message;
import com.geeklabs.templates.util.RequestMapper;
import com.geeklabs.templates.util.ResponseStatus;

@Controller
@RequestMapping(value = RequestMapper.TEMPLATE)
public class TemplateController {

	@Autowired
	private TemplateService templateService;

	@RequestMapping(value = RequestMapper.UPLOAD)
	public void saveTemplate(@ModelAttribute("template") TemplateDto templateDto,
			@RequestParam("file") MultipartFile file, @RequestParam("categoryId") Long categoryId,
			final RedirectAttributes redirectAttributes, Model map) throws IOException {

		if (templateDto.getId() == null) {
			// Save template
			ResponseStatus responseStatus = templateService.saveTemplate(templateDto, file, categoryId);

			if (responseStatus.getStatus() != null && "success".equalsIgnoreCase(responseStatus.getStatus())) {
				redirectAttributes.addFlashAttribute("msg",
						new Message("Template saved successfully.", Message.SUCCESS));
			} else {
				redirectAttributes.addFlashAttribute("msg",
						new Message("Error occured while saving the Template.", Message.ERROR));
			}
		}
	}

	@RequestMapping(value = RequestMapper.GET_TEMPLATES_BY_CATEGORY, method = RequestMethod.GET)
	public @ResponseBody List<TemplateDto> getAllTemplatesByCategoryAndDate(@PathVariable int currentPage,
			@PathVariable Long categoryId) {
		return templateService.getAllTemplatesByCategoryAndDate(currentPage, categoryId);
	}
	
	@RequestMapping(value = RequestMapper.GET_TEMPLATES_BY_CATEGORY_FOR_SERVER, method = RequestMethod.GET, produces = "application/json")
	public @ResponseBody List<TemplateDto> getAllTemplatesByCategoryForServer(@PathVariable Long categoryId) {
		return templateService.getAllTemplatesByCategoryId(categoryId);
	}

	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_TEMPLATES, method = RequestMethod.GET, produces = "application/json")
	public List<TemplateDto> getAllTemplates(@PathVariable int currentPage) {
		return templateService.getAllTemplates(currentPage);
	}
	
	@ResponseBody
	@RequestMapping(value = RequestMapper.GET_TEMPLATES_FOR_SERVER, method = RequestMethod.GET, produces = "application/json")
	public List<TemplateDto> getAllTemplates() {
		return templateService.getAllTemplates();
	}

	@RequestMapping(value = RequestMapper.DELETE_TEMPLATE, method = RequestMethod.DELETE)
	public @ResponseBody ResponseStatus deleteTemplate(@PathVariable Long templateId) {
		return templateService.deleteTemplate(templateId);
	}
	
	@RequestMapping(value = RequestMapper.GET_ALL_TEMPLATES_BY_NAME, method = RequestMethod.GET)
    public @ResponseBody List<TemplateDto> getAllTemplatesByName(@PathVariable String inputSearchText) {
        return templateService.getAllTemplatesByName(inputSearchText);
    }
}
