package com.geeklabs.templates.service;

import com.geeklabs.templates.domain.Document;

public interface DocumentService {
	Document getDocument(String identifier);
}
