package com.geeklabs.templates.service;

import java.util.List;

import com.geeklabs.templates.dto.SubCategoryDto;
import com.geeklabs.templates.util.ResponseStatus;

public interface SubCategoryService {

	ResponseStatus saveSubCategory(SubCategoryDto subCategoryDto);

	List<SubCategoryDto> getAllSubCategories();

	ResponseStatus deleteSubCategory(Long subCategoryId);
}
