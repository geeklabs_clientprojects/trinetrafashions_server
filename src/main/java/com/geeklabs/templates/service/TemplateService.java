package com.geeklabs.templates.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.dto.TemplateDto;
import com.geeklabs.templates.util.ResponseStatus;

public interface TemplateService {

	List<TemplateDto> getAllTemplatesByCategoryAndDate(int currentPage, Long categoryId);

	ResponseStatus saveTemplate(TemplateDto templateDto, MultipartFile file, Long categoryId);

	List<TemplateDto> getAllTemplates(int currentPage);

	ResponseStatus deleteTemplate(Long templateId);

	List<TemplateDto> getAllTemplates();

	List<TemplateDto> getAllTemplatesByCategoryId(Long categoryId);

	List<TemplateDto> getAllTemplatesByName(String inputSearchText);

}
