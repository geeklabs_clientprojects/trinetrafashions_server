package com.geeklabs.templates.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.dto.CategoryDto;
import com.geeklabs.templates.util.ResponseStatus;

public interface CategoryService {

//	ResponseStatus saveCategory(CategoryDto categoryDto);

	ResponseStatus saveCategory(CategoryDto categoryDto, MultipartFile file, String categoryName, Long categoryId);

	List<CategoryDto> getAllCategories();

	ResponseStatus deleteCategory(Long categoryId);

}
