package com.geeklabs.templates.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.geeklabs.templates.domain.User;

public interface UserService extends UserDetailsService {
	
	void setupAppUser();
	
	User getUserByEamailOrUserName(String userName, String email);

}
