package com.geeklabs.templates.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.dto.AddDto;
import com.geeklabs.templates.util.ResponseStatus;

public interface AddService {

	ResponseStatus saveAdd(AddDto addDto, MultipartFile file, String addName, String addUrl, Long addId);

	List<AddDto> getAllAdds();

	ResponseStatus deleteAdd(Long addId);

}
