package com.geeklabs.templates.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.converter.AddConverter;
import com.geeklabs.templates.domain.Add;
import com.geeklabs.templates.dto.AddDto;
import com.geeklabs.templates.repo.AddRepository;
import com.geeklabs.templates.service.AddService;
import com.geeklabs.templates.service.BlobService;
import com.geeklabs.templates.util.ResponseStatus;

@Service
public class AddServiceImpl implements AddService {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private AddRepository addRepository;

	@Autowired
	private BlobService blobService;

	@Override
	@Transactional
	public ResponseStatus saveAdd(AddDto addDto, MultipartFile file, String addName, String addUrl, Long addId) {
		// get instance of response status
		ResponseStatus responseStatus = new ResponseStatus();

		Add add = null;
		// Long addId = addDto.getId();
		if (addId == null || addId == 0) { // Save new add
			addDto.setId(null);
			// save add pic
			try {
				String addPicPath = blobService.storeBlob("Angadi", new MultipartFile[] { file }); // file.getContentType(),
				if (addPicPath != null) {
					addDto.setAddPicPath(addPicPath);
					addDto.setAddName(addName);
					addDto.setAddUrl(addUrl);
				}
			} catch (IOException e) {
				e.printStackTrace();
				responseStatus.setStatus("uploadIssue");
				return responseStatus;
			}

			add = dozerBeanMapper.map(addDto, Add.class);
			add.setAddPicPath(addDto.getAddPicPath());
			add.setAddName(addDto.getAddName());
			add.setAddUrl(addDto.getAddUrl());
			add.setCreatedDate(new Date());
		} else if (addId != null && addId > 0) { // Update existing
													// add
			// update add pic
			try {
				String addPicPath = blobService.storeBlob("Angadi", new MultipartFile[] { file }); // file.getContentType(),
				if (addPicPath != null) {
					addDto.setAddPicPath(addPicPath);
					addDto.setAddName(addName);
					addDto.setAddUrl(addUrl);
				}
			} catch (IOException e) {
				e.printStackTrace();
				responseStatus.setStatus("uploadIssue");
				return responseStatus;
			}
			add = addRepository.findOne(addId);
			dozerBeanMapper.map(addDto, add);
			add.setId(addId);
			add.setAddUrl(addUrl);
			add.setCreatedDate(new Date());
		}
		addRepository.save(add);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<AddDto> getAllAdds() {
		List<Add> adds = (List<Add>) addRepository.findAll();
		List<AddDto> addDtos = AddConverter.convertAddListtoAddDtoList(adds, dozerBeanMapper);
		return addDtos;
	}

	@Override
	public ResponseStatus deleteAdd(Long addId) {
		addRepository.delete(addId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

}
