package com.geeklabs.templates.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.templates.domain.CustomUserDetails;
import com.geeklabs.templates.domain.User;
import com.geeklabs.templates.dto.UserDto;
import com.geeklabs.templates.enums.UserRoles;
import com.geeklabs.templates.repo.UserRepository;
import com.geeklabs.templates.service.UserService;
import com.geeklabs.templates.util.UserConverter;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User userByEamailOrUserName = getUserByEamailOrUserName(username, username);

		if (userByEamailOrUserName != null) {
			List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
			if (UserRoles.SYSTEM_ADMIN.name() == userByEamailOrUserName.getUserRole()) {
				authorities.add(new SimpleGrantedAuthority("ROLE_" + UserRoles.SYSTEM_ADMIN.toString()));
			}

			String password = userByEamailOrUserName.getPassword();
				UserDetails user = new CustomUserDetails(userByEamailOrUserName.getUserName(), password, authorities, userByEamailOrUserName.getId(),
						userByEamailOrUserName.getUserRole());
				return user;
		}
		return null;
	}

	@Override
	@Transactional(readOnly = true)
	public User getUserByEamailOrUserName(String userName, String email) {
		return userRepository.getUserByEmail(email);
	}

	@Override
	@Transactional
	public void setupAppUser() {

		UserDto userDto = new UserDto();
		userDto.setFirstName("Geek");
		userDto.setLastName("Labs");
		userDto.setUserName("geek");
		userDto.setPassword("labs");
		userDto.setEmail("geeklabsapps@gmail.com");
		userDto.setContact(9908069807l);

		User user = userRepository.getUserByEmail(userDto.getEmail());
		if (user == null) {
			user = UserConverter.convertUserDtoToUser(userDto);
			user.setUserRole(UserRoles.SYSTEM_ADMIN.name());
			userRepository.save(user);
		}
	}

	

}
