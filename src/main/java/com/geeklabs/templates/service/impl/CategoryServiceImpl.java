package com.geeklabs.templates.service.impl;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Transactional;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.converter.CategoryConverter;
import com.geeklabs.templates.domain.Category;
import com.geeklabs.templates.dto.CategoryDto;
import com.geeklabs.templates.repo.CategoryRepository;
import com.geeklabs.templates.service.BlobService;
import com.geeklabs.templates.service.CategoryService;
import com.geeklabs.templates.util.ResponseStatus;

@Service
public class CategoryServiceImpl implements CategoryService {
	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private CategoryRepository categoryRepository;

	@Autowired
	private BlobService blobService;

	/*
	 * @Override public ResponseStatus saveCategory(CategoryDto categoryDto) {
	 * ResponseStatus responseStatus = new ResponseStatus(); Category category =
	 * null; Long categoryId = categoryDto.getId(); if (categoryId == null ||
	 * categoryId == 0) { // Save new category categoryDto.setId(null); category
	 * = dozerBeanMapper.map(categoryDto, Category.class);
	 * category.setCreatedDate(new Date()); } else if (categoryId != null &&
	 * categoryId > 0) { // Update existing // category category =
	 * categoryRepository.findOne(categoryId); dozerBeanMapper.map(categoryDto,
	 * category); }
	 * 
	 * categoryRepository.save(category); responseStatus.setStatus("success");
	 * return responseStatus; }
	 */

	@Override
	@Transactional
	public List<CategoryDto> getAllCategories() {
		List<Category> categories = (List<Category>) categoryRepository.findAllByCategoryName();
		List<CategoryDto> categoryDtos = CategoryConverter.convertCategoryListtoCategoryDtoList(categories,
				dozerBeanMapper);
		return categoryDtos;
	}

	@Override
	public ResponseStatus deleteCategory(Long categoryId) {
		categoryRepository.delete(categoryId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public ResponseStatus saveCategory(CategoryDto categoryDto, MultipartFile file, String categoryName,
			Long categoryId) {
		// get instance of response status
		ResponseStatus responseStatus = new ResponseStatus();

		Category category = null;
		// Long categoryId = categoryDto.getId();
		if (categoryId == null || categoryId == 0) { // Save new category
			categoryDto.setId(null);
			// save category pic
			try {
				String categoryPicPath = blobService.storeBlob("Angadi", new MultipartFile[] { file }); // file.getContentType(),
				if (categoryPicPath != null) {
					categoryDto.setCategoryPicPath(categoryPicPath);
					categoryDto.setCategoryName(categoryName);
				}
			} catch (IOException e) {
				e.printStackTrace();
				responseStatus.setStatus("uploadIssue");
				return responseStatus;
			}

			category = dozerBeanMapper.map(categoryDto, Category.class);
			category.setCategoryPicPath(categoryDto.getCategoryPicPath());
			category.setCategoryName(categoryDto.getCategoryName());
			category.setCreatedDate(new Date());
		} else if (categoryId != null && categoryId > 0) { // Update existing
															// category
			// Save category pic
			try {
				String categoryPicPath = blobService.storeBlob("Angadi", new MultipartFile[] { file }); // file.getContentType(),
				if (categoryPicPath != null) {
					categoryDto.setCategoryPicPath(categoryPicPath);
					categoryDto.setCategoryName(categoryName);
				}
			} catch (IOException e) {
				e.printStackTrace();
				responseStatus.setStatus("uploadIssue");
				return responseStatus;
			}
			category = categoryRepository.findOne(categoryId);
			dozerBeanMapper.map(categoryDto, category);
			category.setId(categoryId);
			category.setCreatedDate(new Date());
		}
		categoryRepository.save(category);
		responseStatus.setStatus("success");
		return responseStatus;
	}
	
}
