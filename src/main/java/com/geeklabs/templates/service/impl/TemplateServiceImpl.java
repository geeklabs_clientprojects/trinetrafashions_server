package com.geeklabs.templates.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.geeklabs.templates.converter.TemplateConverter;
import com.geeklabs.templates.domain.Category;
import com.geeklabs.templates.domain.Template;
import com.geeklabs.templates.dto.TemplateDto;
import com.geeklabs.templates.repo.CategoryRepository;
import com.geeklabs.templates.repo.TemplateRepository;
import com.geeklabs.templates.service.BlobService;
import com.geeklabs.templates.service.TemplateService;
import com.geeklabs.templates.util.Pageable;
import com.geeklabs.templates.util.ResponseStatus;

@Service
public class TemplateServiceImpl implements TemplateService {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	@Autowired
	private TemplateRepository templateRepository;
	@Autowired
	private CategoryRepository categoryRepository;
	@Autowired
	private BlobService blobService;

	@Override
	@Transactional
	public List<TemplateDto> getAllTemplatesByCategoryAndDate(int currentPage, Long categoryId) {
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);

		List<Template> templates = templateRepository.getTemplates(pageable, categoryId);
		List<TemplateDto> templateDtos = TemplateConverter.convertTemplatesToTemplateDtos(templates, dozerBeanMapper);
		return templateDtos;
	}

	@Override
	@Transactional
	public ResponseStatus saveTemplate(TemplateDto templateDto, MultipartFile file, Long categoryId) {
		// get instance of response status
		ResponseStatus responseStatus = new ResponseStatus();
		// save template along with pic
		try {
			String templatePicPath = blobService.storeBlob("Angadi", new MultipartFile[] { file }); // file.getContentType(),
																									// file.getBytes()
			if (templatePicPath != null) {
				templateDto.setTemplatePicPath(templatePicPath);
				templateDto.setCategoryId(categoryId);
			}
		} catch (IOException e) {
			e.printStackTrace();
			responseStatus.setStatus("uploadIssue");
			return responseStatus;
		}

//		System.out.println(String.format("receive %s from %s", file.getOriginalFilename(), categoryId));

		Template template = dozerBeanMapper.map(templateDto, Template.class);
		
		template.setCategoryId(templateDto.getCategoryId());
		template.setTemplatePicPath(templateDto.getTemplatePicPath());
		template.setTemplateName(file.getOriginalFilename());
		template.setCreatedDate(new Date());

		templateRepository.save(template);

		// set status
		responseStatus.setStatus("success");
		// return status and use it in front end
		return responseStatus;
	}

	@Override
	@Transactional
	public List<TemplateDto> getAllTemplates(int currentPage) {
		List<TemplateDto> templateDtos = new ArrayList<TemplateDto>();

		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);

		List<Template> templates = (List<Template>) templateRepository.findAllTemplatesByPagable(pageable);
		convertTemplatesToTemplateDtos(templateDtos, templates);
		return templateDtos;
	}

	@Override
	@Transactional
	public ResponseStatus deleteTemplate(Long templateId) {
		ResponseStatus responseStatus = new ResponseStatus();
		templateRepository.delete(templateId);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<TemplateDto> getAllTemplates() {
		List<TemplateDto> templateDtos = new ArrayList<TemplateDto>();

		List<Template> templates = (List<Template>) templateRepository.findAll();
		convertTemplatesToTemplateDtos(templateDtos, templates);
		return templateDtos;
	}

	@Override
	@Transactional
	public List<TemplateDto> getAllTemplatesByCategoryId(Long categoryId) {
		List<TemplateDto> templateDtos = new ArrayList<TemplateDto>();

		List<Template> templates = templateRepository.getTemplatesByCategory(categoryId);
		convertTemplatesToTemplateDtos(templateDtos, templates);
		return templateDtos;
	}

	private void convertTemplatesToTemplateDtos(List<TemplateDto> templateDtos, List<Template> templates) {
		for (Template template : templates) {
			Category category = categoryRepository.findOne(template.getCategoryId());
			TemplateDto templateDto = new TemplateDto();
			templateDto.setCategoryId(template.getCategoryId());
			templateDto.setCategoryName(category.getCategoryName());
			templateDto.setId(template.getId());
			templateDto.setTemplateName(template.getTemplateName());
			templateDto.setTemplatePicPath(template.getTemplatePicPath());
			templateDto.setCreatedDate(template.getCreatedDate());

			templateDtos.add(templateDto);
		}
	}
	
	@Override
	@Transactional
	public List<TemplateDto> getAllTemplatesByName(String inputSearchText) {
		List<Template> templates = (List<Template>) templateRepository.findAllTemplatesByTemplateName(inputSearchText);
		List<TemplateDto> TemplateDtos = TemplateConverter.convertTemplatesToTemplateDtos(templates, dozerBeanMapper);
		return TemplateDtos;
	}
}