package com.geeklabs.templates.service.impl;

import java.util.Date;
import java.util.List;

import javax.jdo.annotations.Transactional;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.geeklabs.templates.converter.SubCategoryConverter;
import com.geeklabs.templates.domain.SubCategory;
import com.geeklabs.templates.dto.SubCategoryDto;
import com.geeklabs.templates.repo.SubCategoryRepository;
import com.geeklabs.templates.service.SubCategoryService;
import com.geeklabs.templates.util.ResponseStatus;

@Service
public class SubCategoryServiceImpl implements SubCategoryService {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private SubCategoryRepository subCategoryRepository;

	@Override
	public ResponseStatus saveSubCategory(SubCategoryDto subCategoryDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		SubCategory subCategory = null;
		Long subCategoryId = subCategoryDto.getId();
		if (subCategoryId == null || subCategoryId == 0) { // Save new
															// subCategory
			subCategoryDto.setId(null);
			subCategory = dozerBeanMapper.map(subCategoryDto, SubCategory.class);
			subCategory.setCreatedDate(new Date());
		} else if (subCategoryId != null && subCategoryId > 0) { // Update
																	// existing
																	// subCategory
			subCategory = subCategoryRepository.findOne(subCategoryId);
			dozerBeanMapper.map(subCategoryDto, subCategory);
		}

		subCategoryRepository.save(subCategory);
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public List<SubCategoryDto> getAllSubCategories() {
		List<SubCategory> categories = (List<SubCategory>) subCategoryRepository.findAll();
		List<SubCategoryDto> subCategoryDtos = SubCategoryConverter
				.convertSubCategoryListtoSubCategoryDtoList(categories, dozerBeanMapper);
		return subCategoryDtos;
	}

	@Override
	public ResponseStatus deleteSubCategory(Long subCategoryId) {
		subCategoryRepository.delete(subCategoryId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}
}
