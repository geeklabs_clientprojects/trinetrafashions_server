package com.geeklabs.templates.enums;

public enum UserStatus {
	
	NEW, ACTIVE, BANNED, BLOCKED, INACTIVE;
}
