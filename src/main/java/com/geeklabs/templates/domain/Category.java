package com.geeklabs.templates.domain;

import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Category {

	@Id
	private Long id;
	@Index
	private String categoryName;
	private Date createdDate;
	private String categoryPicPath;
	
	public String getCategoryPicPath() {
		return categoryPicPath;
	}
	public void setCategoryPicPath(String categoryPicPath) {
		this.categoryPicPath = categoryPicPath;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
}
