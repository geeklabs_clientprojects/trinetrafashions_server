package com.geeklabs.templates.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.templates.domain.Template;
import com.geeklabs.templates.dto.TemplateDto;

public class TemplateConverter {

	public static List<TemplateDto> convertTemplatesToTemplateDtos(List<Template> templates, DozerBeanMapper dozerBeanMapper) {

		List<TemplateDto> templateDtos = new ArrayList<>();

		for (Template template : templates) {
			TemplateDto templateDto = dozerBeanMapper.map(template, TemplateDto.class);
			templateDtos.add(templateDto);
		}

		return templateDtos;

	}
}
