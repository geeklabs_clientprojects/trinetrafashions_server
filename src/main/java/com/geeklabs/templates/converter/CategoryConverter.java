package com.geeklabs.templates.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.templates.domain.Category;
import com.geeklabs.templates.dto.CategoryDto;

public class CategoryConverter {

	public static Category convertCategoryDtoListToCategoryList(CategoryDto categoryDto, Category category) {

		category.setId(categoryDto.getId());
		category.setCategoryName(categoryDto.getCategoryName());
		category.setCreatedDate(categoryDto.getCreatedDate());

		return category;

	}

	public static List<CategoryDto> convertCategoryListtoCategoryDtoList(List<Category> categories,
			DozerBeanMapper dozerBeanMapper) {
		ArrayList<CategoryDto> categoryDtos = new ArrayList<>();
		for (Category category : categories) {
			CategoryDto categoryDto = dozerBeanMapper.map(category, CategoryDto.class);
			categoryDtos.add(categoryDto);
		}
		return categoryDtos;
	}

}
