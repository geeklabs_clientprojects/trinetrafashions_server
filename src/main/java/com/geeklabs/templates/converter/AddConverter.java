package com.geeklabs.templates.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.templates.domain.Add;
import com.geeklabs.templates.dto.AddDto;

public class AddConverter {

	public static List<AddDto> convertAddListtoAddDtoList(List<Add> adds,
			DozerBeanMapper dozerBeanMapper) {
		ArrayList<AddDto> addDtos = new ArrayList<>();
		for (Add add : adds) {
			AddDto addDto = dozerBeanMapper.map(add, AddDto.class);
			addDtos.add(addDto);
		}
		return addDtos;
	}

}
