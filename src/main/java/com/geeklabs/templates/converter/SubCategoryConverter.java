package com.geeklabs.templates.converter;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.geeklabs.templates.domain.SubCategory;
import com.geeklabs.templates.dto.SubCategoryDto;

public class SubCategoryConverter {

	public static List<SubCategoryDto> convertSubCategoryListtoSubCategoryDtoList(List<SubCategory> subCategories,
			DozerBeanMapper dozerBeanMapper) {

		ArrayList<SubCategoryDto> categoryDtos = new ArrayList<SubCategoryDto>();
		for (SubCategory subCategory : subCategories) {
			SubCategoryDto subCategoryDto = dozerBeanMapper.map(subCategory, SubCategoryDto.class);
			categoryDtos.add(subCategoryDto);
		}

		return categoryDtos;

	}
}
