(function() {
	'use strict';
	angular.module('TemplatesApp').controller('PwdCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session) {
		debugger;
		
		$scope.changePwd = function(changePwdVO) {
			debugger;
			$http({
				url : "/user/changepwd",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : changePwdVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Password changed successfully.')
				} 
			});
		};
		
		$scope.getPwd = function(getPwdVO) {
			debugger;
			$http({
				url : "/user/forgetpwd",
				method : 'POST',
				dataType : 'json',
				'headers' : {
					'Content-Type' : 'application/json'
				},
				data : getPwdVO
			}).then(function(dataResponse) {
				debugger;
				if (dataResponse.data.status == "success") {
					notify('Password has been sent to registered email.')
					$state.transitionTo("home");
				} 
			});
		};
		
	});
})();