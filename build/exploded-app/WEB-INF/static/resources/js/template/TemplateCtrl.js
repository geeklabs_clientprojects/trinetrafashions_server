/*$(function() {
		$(window).scroll(function() {
			if ($(window).scrollTop() + $(window).height() >= $('#templates_table').offset().top + $('#templates_table').height() - 200) {
				angular.element($('#TemplateCtrl')).scope().startPagination();
			}
		});
	});*/

(function() {
	'use strict';

	angular.module('TemplatesApp').controller('TemplateCtrl', function($rootScope, $scope, $state,$http, notify, Session, Upload) {
		$scope.categories = [];
		$scope.templates = [];
		$scope.pageNo = 0;
		$scope.isPaginationStart = false;
		$scope.isPaginationCompleted = false;
		
		
		$scope.startPagination = function() {
			debugger;
			if (!$scope.isPaginationStart && !$scope.isPaginationCompleted) {
				$scope.isPaginationStart = true;
				$scope.paginate();
			}
		};
		
		$scope.paginate = function() {
			debugger;
			$scope.pageNo = $scope.pageNo + 1;
			var path = "/template/get/" + $scope.pageNo ;
			$http({
				url : path,
				method : 'GET',
				dataType : 'json',
			}).then(function(dataResponse) {
				debugger;
				$scope.isPaginationStart = false;
				if (null == dataResponse || dataResponse.data.length == 0) {
					// empty result means pagination not required
					// more logically we need the total pages for particular
					// request
					// to stop unwanted pagination request
					$scope.isPaginationCompleted = true;
					$scope.pageNo = $scope.pageNo - 1;
					return;
				}

				var templateList = dataResponse.data;
				// newly coming templates should be add to the old
				$scope.templates.push.apply($scope.templates, templateList);
			});
		};
		
		// upload later on form submit or something similar
	    $scope.saveTemplateWithCategory = function() {
	    	debugger;
	      if ($scope.file && !$scope.file.$error) {
	        $scope.upload($scope.file);
	        $scope.getTemplates();
	      } else {
	    	  alert('Please choose a Category and File before Upload');
	      }
	    };

		// upload on file select or drop
	    $scope.upload = function (file) {
	    	debugger;
	        Upload.upload({
	            url: '/template/upload',
	            fields: {'categoryId': $scope.categoryItem.id},
	            file: file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	        }).success(function (data, status, headers, config) {
	        	debugger;
	            console.log('File ' + config.file.name + 'uploaded. Response: ' + data);
	            alert('File ' + config.file.name + ' uploaded successfully.');
	            $scope.getTemplates();
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	            $scope.getTemplates();
	            // alert('Failed to upload file ' + config.file.name);
	        })
	        $scope.getTemplates();
	    };
	    
	    $scope.getCategories = function() {
			debugger;
			$http({
				url : "/category/get",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.categories = data;
				$scope.categories.push({"categoryName":"ALL"});
			});
		};
		$scope.getTemplates = function() {
			debugger;
			$http({
				/* url : "/template/get/" + $scope.pageNo, */
				url : "/template/get",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				debugger;
				var data = dataResponse.data;
				$scope.templates = data;
				
			});
		};
		
		$scope.filterList = function() {
			debugger;
				if ($scope.categoryItem.categoryName == "ALL") {
					$scope.getTemplates();
				} else {
				$http({
					url : "/template/get/template/" + $scope.categoryItem.id,
					method : 'GET',
					dataType : 'json'
				}).then(function(dataResponse) {
					debugger;
					var data = dataResponse.data;
					$scope.templates = data;
				});
			}
		};
		
		$scope.deleteTemplate = function(templateId) {
			debugger;
			var result = confirm("Are you sure want to delete?");
			if (result == true) {
				$http({
					url : "/template/delete/" + templateId,
					method : 'DELETE',
					dataType : 'json'
				}).then(function(dataResponse) {
					debugger;
					if (dataResponse.data.status == "success") {
						notify('Template deleted successfully');
						$scope.getTemplates();
					} else {
						notify('Error occured While deleting Template');
					}
				});
				}
			};
	    $scope.getCategories();
//	    $scope.getTemplates();
//	    $scope.paginate();
			
	});
})();