
(function() {
	'use strict';
	angular.module('TemplatesApp').controller('HomeCtrl', function($rootScope, $scope, $state,$http, notify, Session, Upload) {
		$scope.categories = [];
		
		// upload later on form submit or something similar
	    $scope.saveCategoryWithFile = function() {
	    	debugger;
	      if ($scope.file && !$scope.file.$error) {
	        $scope.upload($scope.file);
	      }
	    };

		// upload on file select or drop
	    $scope.upload = function (file) {
	    	debugger;
	        Upload.upload({
	            url: '/category/upload',
	            fields: {'categoryName': $scope.categoryName},
	            file: file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	        }).success(function (data, status, headers, config) {
	        	debugger;
	            console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })
	    };
			
	});
})();