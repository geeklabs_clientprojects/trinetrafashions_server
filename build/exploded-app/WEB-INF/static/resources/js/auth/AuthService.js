angular.module('TemplatesApp').factory('AuthService', function($http, Session) {
	var authService = {};

	authService.isAuthenticated = function() {
		return Session.userId != "";
	};

	authService.isAuthorized = function(authorizedRoles) {
		if (!angular.isArray(authorizedRoles)) {
			authorizedRoles = [ authorizedRoles ];
		}
		return (authService.isAuthenticated() && authorizedRoles.indexOf(Session.userRole) !== -1);
	};

	return authService;
});