//Fetches Model and minds to view
//Can use services to make http calls.

// Inspired from https://medium.com/opinionated-angularjs/techniques-for-authentication-in-angularjs-applications-7bbf0346acec

(function() {
	'use strict';
	var app = angular.module('TemplatesApp').controller('IndexCtrl', function($rootScope, $scope, $state, $http, notify, AUTH_EVENTS, AuthService, Session,UserInfoService) {

		var categories = [];
		var templates = [];
		var templatePicPath;
		
		$scope.showImagePopup = function(templatePicPath) {
			$scope.templatePicPath = templatePicPath;
			$(".template-popup-modal").modal("show");
		};
		
		$scope.getCategories = function() {
    			$http({
    				url : "/category/get/",
    				method : 'GET',
    				dataType : 'json'
    			}).then(function(dataResponse) {
    				var data = dataResponse.data;
    				$scope.categories = data;
    			});
    		};
    		
		$scope.backToHome = function() {
			$state.transitionTo("index");
			$scope.isBackVisible =  false;
			location.reload();
		};
    		
        $scope.getAdds = function() {
			$http({
				url : "/add/get",
				method : 'GET',
				dataType : 'json'
			}).then(function(dataResponse) {
				var data = dataResponse.data;
				$scope.adds = data;
			});
		};
		
		$scope.getAdds();
        $scope.getCategories();
	});
})();